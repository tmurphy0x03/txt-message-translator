package org.translator;






import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Contacts.People;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class SendSMS extends ListActivity{
	
	private Cursor contactsCursor;
	
	/* Called on creation */
	public void onCreate(Bundle SavedInstanceState){
		super.onCreate(SavedInstanceState);
		
		/* Set the UI to send_sms.xml */
		setContentView(R.layout.send_sms);
		
		/* Register a context menu to be shown for a list view */ 
		registerForContextMenu(getListView());		

		/* Contacts columns to return */
		String[] contactCols = new String[] {People._ID, People.NAME, People.NUMBER};

		/* Query contacts and return in ascending order */
		Uri contacts =  People.CONTENT_URI; 
		contactsCursor = managedQuery(contacts, contactCols, null, null, People.NAME + " ASC");
		
		/* Columns to use */
		String[] number = new String[]{People.NAME, People.NUMBER};	

		/* Where to display the columns */
		int[] toNumber = new int[]{R.id.name, R.id.number};	
		
		/* Map the contactsCursor to a list view */
		SimpleCursorAdapter numbers = new SimpleCursorAdapter(this, R.layout.send_row, contactsCursor, number, toNumber);
		setListAdapter(numbers);
	}
	
	/* An item in the list is selected */
	protected void onListItemClick(ListView l, View v, int position, long id) {
    	super.onListItemClick(l, v, position, id);
    	/* Create a cursor and move it to the selected contact */
    	Cursor c = contactsCursor;
        c.moveToPosition(position);
        
        /* Add the name and number of the contact selected to a Bundle */
    	Bundle bundle = new Bundle();    	
		bundle.putString("number", c.getString(c.getColumnIndexOrThrow("number")));	
		bundle.putString("name", c.getString(c.getColumnIndexOrThrow("name")));
		
		/* Put the Bundle into an Intent */
		Intent i = new Intent();
		i.putExtras(bundle);
		
		/* Return success */
		setResult(RESULT_OK, i);
		
		/* Close the activity */
		finish();			
    }
}
