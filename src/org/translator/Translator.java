package org.translator;

/**
 * @author Anthony Murphy & Daniel Keenan
 */

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;

import org.translator.dao.DatabaseAdapter;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.os.Bundle;
import android.telephony.gsm.SmsManager;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;


public class Translator extends Activity {
	private static final int DB_ADD = Menu.FIRST;	
	private static final int IMPORT_SMS = Menu.FIRST+1;
	private static final int SEND_SMS = Menu.FIRST+2;
	private static final int ABOUT = Menu.FIRST+3;
	private static final int ADD_DICTIONARY = 0;	
	private static final int IMPORT = 1;
	private static final int SEND = 2;
	private static final int ABOUT_WIN = 3;
	private Toast t_toast; 
	private View t_textView;  
	private LinearLayout t_layout;
	private ImageView t_imageView;
	private DatabaseAdapter db_adapter;
	public EditText et_txtText;
	private Cursor db_cursor;
	private LinkedList<String> m_words = new LinkedList<String>();
	private char m_symbols[] = {'.' , '!' , '?' , '"', ' ', '�', ',' , ':', ';', ')', '-', '�', '`', '$', '%', '^', '&', '*', '(', '_', '+', '=', '{', '}', '[', ']', ';', '@', '~', '#', '|','<','>','/',0x5c, '\n'};

	/**
	 * Called on creation of the Translator activity
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		/* Set the UI to translate.xml */
		setContentView(R.layout.translate);		
		db_adapter = new DatabaseAdapter(this);

		/* Create the Database */
		try{
			db_adapter.createDatabase();  
		} catch (IOException ioe){
			throw new Error("Unable to create database");
		}

		/* Open the Database */
		try{
			db_adapter.openDatabase();
		} catch(SQLException sqle){
			throw new Error("Unable to open the database");
		}

		/* Initialise Buttons */
		final Button toEnglish = (Button) findViewById(R.id.toEnglish);
		final Button toText = (Button) findViewById(R.id.toText);
		final Button clear = (Button) findViewById(R.id.clear);

		/* Set up button listeners */
		toEnglish.setOnClickListener(toEnglishListener); 
		toText.setOnClickListener(toTextListener);
		clear.setOnClickListener(clearListener);  
	}  	

	/**
	 * Translate to English button listener
	 */
	private OnClickListener toEnglishListener = new OnClickListener(){            
		public void onClick(View v) {
			translate("txteng");
		}
	};  

	/**
	 *  Translate to Text button listener 
	 */
	private OnClickListener toTextListener = new OnClickListener(){            
		public void onClick(View v) {
			translate("engtxt");
		}
	};

	/**
	 *  Clear button listener 
	 */
	private OnClickListener clearListener = new OnClickListener(){            
		public void onClick(View v) {
			et_txtText = (EditText) findViewById(R.id.txt);	   
			et_txtText.setText("");        	
		}          
	};  

	/**
	 * Create the Options menu
	 */
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		menu.add(0, DB_ADD, 0, R.string.db_add);		
		menu.add(0, IMPORT_SMS, 0, R.string.import_sms);
		menu.add(0, SEND_SMS, 0, R.string.send_sms);
		menu.add(0, ABOUT, 0, R.string.about);
		return true;
	}

	/**
	 * An item in the options menu is selected
	 */
	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		Intent i;
		switch(item.getItemId()) {
		/* Add To Dictionary option was selected */
		case DB_ADD:
			i = new Intent(this, AddWord.class);
			startActivityForResult(i, ADD_DICTIONARY);
			return true; 

			/* About option was selected */
		case ABOUT:
			i = new Intent(this, About.class);
			startActivityForResult(i, ABOUT_WIN);
			return true;

			/* Copy from Inbox option was selected */
		case IMPORT_SMS:
			i = new Intent(this, ImportSMS.class);
			startActivityForResult(i, IMPORT);
			return true;

			/* Send to Contact option was selected */
		case SEND_SMS:
			i = new Intent(this, SendSMS.class);
			startActivityForResult(i, SEND);
			return true;
		}
		return super.onMenuItemSelected(featureId, item);
	}

	/**
	 * Returned from an activity
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
		super.onActivityResult(requestCode, resultCode, intent);
		switch(requestCode) {		
		case ADD_DICTIONARY:
			/* Returned from the Add to Dictionary activity */
			if(resultCode == RESULT_OK){
				Bundle extras = intent.getExtras();
				String textBox = extras.getString(DatabaseAdapter.KEY_TXT);
				String englishBox = extras.getString(DatabaseAdapter.KEY_ENG);
				LinkedList<String> textWords = new LinkedList<String>();
				LinkedList<String> englishWords = new LinkedList<String>();

				/* Remove multiple spaces */
				while(textBox.contains("  ")){ 
					textBox = textBox.replace("  ", " "); 
				}
				while(englishBox.contains("  ")){ 
					englishBox = englishBox.replace("  ", " "); 
				}

				/* if one of the text boxes was left blank, notify the user */
				if(textBox.equals("") || textBox.equals(" ") || englishBox.equals("") || englishBox.equals(" ")){
					notifyUser(1, "Nothing was added to the dictionary. Both fields must contain values!");
				}

				/* otherwise continue */
				else{		
					/* If the user left a ' ' (i.e. space) at the end of an entry, then ignore it */
					if(textBox.charAt(textBox.length()-1) == ' '){
						textBox = textBox.substring(0, textBox.length()-1);
					}
					if(englishBox.charAt(englishBox.length()-1) == ' '){
						englishBox = englishBox.substring(0, englishBox.length()-1);
					}

					textWords = parseTextBeingAddedToDictionary(textBox);
					englishWords = parseTextBeingAddedToDictionary(englishBox);

					/* Add translation to the dictionary & notify the user */
					if(textWords != null && englishWords != null){	
						db_adapter.addTranslation(textWords.get(0), textWords.get(1), textWords.get(2), textWords.get(3), englishWords.get(0), englishWords.get(1), englishWords.get(2), englishWords.get(3));
						notifyUser(0, "'" + textBox + "' was successfully added to the dictionary");

					}
					/* Clear the arrays */
					englishWords.clear();
					textWords.clear();
				}
			}

		case ABOUT_WIN:
			/* Returned from the About activity */
			break;

		case IMPORT:
			/* Returned from the Copy from Inbox activity */
			if(resultCode == RESULT_OK){
				/* Put the message into the translate box */
				Bundle extras = intent.getExtras();
				et_txtText = (EditText) findViewById(R.id.txt);	   
				et_txtText.setText(extras.getString("body")); 
			}
			break;

		case SEND:
			/* Returned from the Send to Contact activity */
			if(resultCode == RESULT_OK){
				/* Get the name, number and message */
				Bundle extras = intent.getExtras();
				String number = extras.getString("number");
				String name = extras.getString("name");
				et_txtText = (EditText) findViewById(R.id.txt);				
				String msg = et_txtText.getText().toString();				

				/* Try to send the text message */
				try{
					SmsManager send = SmsManager.getDefault();

					/* Divide the message into parts, none bigger than the maximum SMS message size */
					ArrayList<String> message = send.divideMessage(msg);
					send.sendMultipartTextMessage(number, null, message, null, null);

					/* Notify User and clear the translation box */
					notifyUser(0, "Text Message was sent to " + name);
					et_txtText.setText("");					

				}
				/* Catch a Runtime Exception */
				catch(RuntimeException r){
					notifyUser(1, "Please enter a message");
				}
			}
			else notifyUser(1, "Sending Cancelled!");
			break;
		}
	}

	/**
	 * Parse a string that is being added to the dictionary and return it 
	 * as a list and also validate that it's a valid entry. Returns null if
	 * the input was an invalid entry 
	 * @param text
	 * @return
	 */
	public LinkedList<String> parseTextBeingAddedToDictionary(String text){
		int start = 0;
		int end = 0;
		LinkedList<String> list = new LinkedList<String>();
		/* Parse the Txt Message Language entry */
		for(int i = 0; i < text.length(); i++){
			for(int j = 0; j < m_symbols.length; j++){

				/* Check for symbols */
				if(text.charAt(i) == m_symbols[j] && text.charAt(i) != ' '){
					notifyUser(1, "'" + text + "' was not added to the dictionary. Only words allowed!");
					return null;
				}

				if(text.charAt(start) == 0x27){
					notifyUser(1, "'" + text + "' was not added to the dictionary. Only words allowed!");
					return null;
				}

				/* When reached the end of the entry */
				if(list.size() < 4 && i == text.length()-1 && j == m_symbols.length-1){
					end = i;
					if(text.charAt(end) == 0x27){
						notifyUser(1, "'" + text + "' was not added to the dictionary. Only words allowed!");
						return null;
					}
					list.add(text.substring(start));
					return null;
				}

				/* Add words to the Array of Txt Message Langauge words */
				if(list.size() < 4 && text.charAt(i) == ' ' && i != text.length()-1){
					end = i;
					if(text.charAt(end) == 0x27){
						notifyUser(1, "'" + text + "' was not added to the dictionary. Only words allowed!");
						return null;
					}
					list.add(text.substring(start, end));
					start = i+1;
					break;
				}

				/* Check that no more than 4 words were entered */
				if(list.size() >= 4){
					notifyUser(1, "'" + text + "' was not added to the dictionary. Add up to 4 words only!"); 
					return null;
				}
			}
		}
		/* Put null in the remainder of the columns so that there is exactly 4 entries */
		while(list.size() < 4){
			list.add("");
		}

		return list;

	}
	/**
	 * Translate function
	 * @param direction - the direction of the translation e.g. Txt to English
	 */
	private void translate(String direction){
		/* Read values in the text box into a string */
		et_txtText = (EditText) findViewById(R.id.txt);
		String result = et_txtText.getText().toString();

		/* Remove multiple spaces */
		while(result.contains("  ")){ 
			result = result.replace("  ", " "); 
		}

		/* If nothing was entered into the text box notify the user */
		if(result.equals("") || result.equals(" ")){
			notifyUser(1, "Nothing entered!");
		}

		/* Otherwise text was entered */
		else{
			if(result.charAt(0) == ' '){
				result = result.substring(1);
			}

			/* Parse the string into a list */
			m_words = parseTextForTranslation(result);

			/* Translate words in the list */
			result = getTranslationOfList(m_words, direction);			

			/* Display the resulting translation */
			et_txtText.setText(result);

			/* Clear the words array */
			m_words.clear();
		} 
	}

	/**
	 * Display a notification to the user
	 * @param notificationType
	 * @param message
	 */
	private void notifyUser(int notificationType, String message){		
		switch(notificationType){		
		case 0:	
			/* Success notification */
			t_toast = new Toast(Translator.this);
			t_toast = Toast.makeText(Translator.this, message, Toast.LENGTH_LONG);
			t_textView = t_toast.getView(); 
			t_layout = new LinearLayout(Translator.this); 
			t_layout.setOrientation(LinearLayout.HORIZONTAL); 
			t_imageView = new ImageView(Translator.this); 
			t_imageView.setImageResource(R.drawable.green_tick); 
			t_layout.addView(t_imageView); 
			t_layout.addView(t_textView); 
			t_toast.setView(t_layout); 
			t_toast.setGravity(Gravity.TOP|Gravity.LEFT, 0, 50);
			t_toast.show(); 
			break;			
		case 1:	
			/* Error notification */
			t_toast = new Toast(Translator.this);
			t_toast = Toast.makeText(Translator.this, message, Toast.LENGTH_LONG);
			t_textView = t_toast.getView(); 
			t_layout = new LinearLayout(Translator.this); 
			t_layout.setOrientation(LinearLayout.HORIZONTAL); 
			t_imageView = new ImageView(Translator.this); 
			t_imageView.setImageResource(R.drawable.red_x); 
			t_layout.addView(t_imageView); 
			t_layout.addView(t_textView); 
			t_toast.setView(t_layout); 
			t_toast.setGravity(Gravity.TOP|Gravity.LEFT, 0, 50);
			t_toast.show(); 
			break;
		}		
	}

	/**
	 * Parse a string and split into words with each words
	 * trailing symbol (e.g. ' ' or '.') and put into a list
	 * @param s
	 * @return the list of words
	 */
	private LinkedList<String> parseTextForTranslation(String s){
		int start = 0;
		int end = 0;
		LinkedList<String> list = new LinkedList<String>();

		/* Parse through the string */
		Parsing:
			for(int i = 0; i < s.length(); i++){
				for(int j = 0; j < m_symbols.length; j++){
					if(s.charAt(i) == m_symbols[j]){
						/* Final character */
						if(i == s.length()-1){
							end = i;
							if(s.charAt(start) == 0x27){
								start++;
								list.add("'");
							}
							if(i > 0 && start < end-1){
								if(s.charAt(end-1) == 0x27){
									end--;
									list.add(s.substring(start, end) + "'");
									list.add(m_symbols[j] + "");
								}
								else list.add(s.substring(start, end) + m_symbols[j]);
							}
							else list.add(s.substring(start, end) + m_symbols[j]);

							break Parsing;
						}

						/* Add the current word to words array */
						else if(i != s.length()-1){
							end = i;
							if(s.charAt(start) == 0x27){
								start++;
								list.add("'");
							}

							if(i > 0 && start < end-1){
								if(s.charAt(end-1) == 0x27){
									end--;
									list.add(s.substring(start, end) + "'");
									list.add(m_symbols[j] + "");
								}
								else list.add(s.substring(start, end) + m_symbols[j]);
							}
							else list.add(s.substring(start, end) + m_symbols[j]);

							start = i + 1;
							break;
						}							
					}
				}

				/* Add full stop at end */
				if(i == s.length()-1){
					end = i+1;
					if(s.charAt(start) == 0x27){
						start++;
						list.add("'");
					}

					if(s.charAt(end-1) == 0x27 && start < end-1){
						end--;
						list.add(s.substring(start, end) + "'");
						list.add(".");
					}
					else list.add(s.substring(start, end) + ".");

					break Parsing;
				}					
			}

		return list;
	}

	/**
	 * Iterate through a list and translate each element. Return the full
	 * resulting transation as a String
	 * @param list
	 * @param direction
	 * @return
	 */
	private String getTranslationOfList(LinkedList<String> list, String direction){
		String s = new String();
		String result = new String();		

		Boolean resultFound = false;
		Cursor testCursor;
		/* Get translation of words in words array */
		for(int i = 0; i < m_words.size(); i++){
			/* Check to see if the word is in the dictionary */
			testCursor = db_adapter.getTranslation(m_words.get(i).substring(0, m_words.get(i).length()-1).toLowerCase(), null, null, null, direction, true);

			/* Check for 4 word translation */
			if(i <= m_words.size()-4 && !resultFound && testCursor.getCount() > 0){
				db_cursor = db_adapter.getTranslation(m_words.get(i).substring(0, m_words.get(i).length()-1).toLowerCase(), m_words.get(i+1).substring(0, m_words.get(i+1).length()-1).toLowerCase(), m_words.get(i+2).substring(0, m_words.get(i+2).length()-1).toLowerCase(), m_words.get(i+3).substring(0, m_words.get(i+3).length()-1).toLowerCase(), direction, false);

				/* If a result was returned */
				if(db_cursor.getCount() > 0){	
					/* 1 word result */
					if(db_cursor.getString(5) == null){
						s += db_cursor.getString(4) + m_words.get(i+3).charAt(m_words.get(i+3).length()-1);
					}
					/* 2 word result */
					else if(db_cursor.getString(6) == null){
						s += db_cursor.getString(4) + m_words.get(i).charAt(m_words.get(i).length()-1) + db_cursor.getString(5) + m_words.get(i+3).charAt(m_words.get(i+3).length()-1);
					}
					/* 3 word result */
					else if(db_cursor.getString(7) == null){
						s += db_cursor.getString(4) + m_words.get(i).charAt(m_words.get(i).length()-1) + db_cursor.getString(5) + m_words.get(i).charAt(m_words.get(i).length()-1)  + db_cursor.getString(6) + m_words.get(i+3).charAt(m_words.get(i+3).length()-1);
					}
					/* 4 word result */
					else{
						s += db_cursor.getString(4) + m_words.get(i).charAt(m_words.get(i).length()-1) + db_cursor.getString(5) + m_words.get(i).charAt(m_words.get(i).length()-1)  + db_cursor.getString(6) + m_words.get(i).charAt(m_words.get(i).length()-1)  + db_cursor.getString(7) + m_words.get(i+3).charAt(m_words.get(i+3).length()-1);
					}
					/* Remove the next 3 words */
					m_words.remove(i+3);
					m_words.remove(i+2);
					m_words.remove(i+1);
					resultFound = true;
				}
			}
			/* If no match yet, check for 3 word translation */
			if(i <= m_words.size()-3 && !resultFound && testCursor.getCount() > 0){
				db_cursor = db_adapter.getTranslation(m_words.get(i).substring(0, m_words.get(i).length()-1).toLowerCase(), m_words.get(i+1).substring(0, m_words.get(i+1).length()-1).toLowerCase(), m_words.get(i+2).substring(0, m_words.get(i+2).length()-1).toLowerCase(), null, direction, false);
				/* If a result was returned */
				if(db_cursor.getCount() > 0){	
					/* 1 word result */
					if(db_cursor.getString(5) == null){
						s += db_cursor.getString(4) + m_words.get(i+2).charAt(m_words.get(i+2).length()-1);
					}
					/* 2 word result */
					else if(db_cursor.getString(6) == null){
						s += db_cursor.getString(4) + m_words.get(i).charAt(m_words.get(i).length()-1) + db_cursor.getString(5) + m_words.get(i+2).charAt(m_words.get(i+2).length()-1);
					}
					/* 3 word result */
					else if(db_cursor.getString(7) == null){
						s += db_cursor.getString(4) + m_words.get(i).charAt(m_words.get(i).length()-1) + db_cursor.getString(5) + m_words.get(i).charAt(m_words.get(i).length()-1)  + db_cursor.getString(6) + m_words.get(i+2).charAt(m_words.get(i+2).length()-1);
					}
					/* 4 word result */
					else {
						s += db_cursor.getString(4) + m_words.get(i).charAt(m_words.get(i).length()-1) + db_cursor.getString(5) + m_words.get(i).charAt(m_words.get(i).length()-1)  + db_cursor.getString(6) + m_words.get(i).charAt(m_words.get(i).length()-1)  + db_cursor.getString(7) + m_words.get(i+2).charAt(m_words.get(i+2).length()-1);							
					}
					/* Remove the next 2 words */
					m_words.remove(i+2);
					m_words.remove(i+1);
					resultFound = true;
				}


			}
			/* If no match yet, check for 2 word translation */
			if(i <= m_words.size()-2 && !resultFound && testCursor.getCount() > 0){
				db_cursor = db_adapter.getTranslation(m_words.get(i).substring(0, m_words.get(i).length()-1).toLowerCase(), m_words.get(i+1).substring(0, m_words.get(i+1).length()-1).toLowerCase(), null, null, direction, false);
				/* If a result was returned */
				if(db_cursor.getCount() > 0){
					/* 1 word result */
					if(db_cursor.getString(5) == null){
						s += db_cursor.getString(4) + m_words.get(i+1).charAt(m_words.get(i+1).length()-1);
					}
					/* 2 word result */
					else if(db_cursor.getString(6) == null){
						s += db_cursor.getString(4) + m_words.get(i).charAt(m_words.get(i).length()-1) + db_cursor.getString(5)+ m_words.get(i+1).charAt(m_words.get(i+1).length()-1);
					}
					/* 3 word result */
					else if(db_cursor.getString(7) == null){
						s += db_cursor.getString(4) + m_words.get(i).charAt(m_words.get(i).length()-1) + db_cursor.getString(5) + m_words.get(i).charAt(m_words.get(i).length()-1)  + db_cursor.getString(6) + m_words.get(i+1).charAt(m_words.get(i+1).length()-1);
					}
					/* 4 word result */
					else{
						s += db_cursor.getString(4) + m_words.get(i).charAt(m_words.get(i).length()-1) + db_cursor.getString(5) + m_words.get(i).charAt(m_words.get(i).length()-1)  + db_cursor.getString(6) + m_words.get(i).charAt(m_words.get(i).length()-1)  + db_cursor.getString(7) + m_words.get(i+1).charAt(m_words.get(i+1).length()-1);
					}
					m_words.remove(i+1);
					resultFound = true;
				}
			}

			/* If no match yet, get the single word translation */
			if(!resultFound && testCursor.getCount() > 0){
				db_cursor = db_adapter.getTranslation(m_words.get(i).substring(0, m_words.get(i).length()-1).toLowerCase(), null, null, null, direction, false);
				/* If a result was returned */
				if(db_cursor.getCount() > 0){
					/* 1 word result */
					if(db_cursor.getString(5) == null){
						s += db_cursor.getString(4) + m_words.get(i).charAt(m_words.get(i).length()-1);
					}
					/* 2 word result */
					else if(db_cursor.getString(6) == null){
						s += db_cursor.getString(4) + " " + db_cursor.getString(5)+ m_words.get(i).charAt(m_words.get(i).length()-1);
					}
					/* 3 word result */
					else if(db_cursor.getString(7) == null){
						s += db_cursor.getString(4) + " " + db_cursor.getString(5) + " " + db_cursor.getString(6) + m_words.get(i).charAt(m_words.get(i).length()-1);
					}
					/* 4 word result */
					else{
						s += db_cursor.getString(4) + " " + db_cursor.getString(5) + " "  + db_cursor.getString(6) + " "  + db_cursor.getString(7) + m_words.get(i).charAt(m_words.get(i).length()-1);
					}
					resultFound = true;
				}
			}

			/* No translation in dictionary */
			if(!resultFound){
				s += m_words.get(i);
			}
			resultFound = false;
		}


		/* Parse through the result and perform grammar correction */
		result = "";
		for(int i = 0; i < s.length()-1; i++){
			/* Put first letter of translation to upper-case */
			if(i == 0){
				result += s.substring(0, 1).toUpperCase();
			}

			else if(i > 2){
				if(s.charAt(i-1) == ' '){
					/* Put capital letter after '.' '!' '?' (i.e. start of new sentence) */
					if(s.charAt(i-2) == '.' || s.charAt(i-2) == '!' || s.charAt(i-2) == '?'){
						result += s.substring(i, i+1).toUpperCase();
					}
					else{
						result+= s.substring(i, i+1);
					}				
				}
				else{
					result+= s.substring(i, i+1);
				}				
			}
			else{
				result+= s.substring(i, i+1);
			}				
		}

		/* Put full stop at end of translation if no other symbol there */
		if((s.charAt(s.length()-1) == ' ' || s.charAt(s.length()-1) == '\n')
				&& (s.charAt(s.length()-2) != '.' && s.charAt(s.length()-2) != '?' && s.charAt(s.length()-2) != '!')){
			result+= '.';
		}
		else if((s.charAt(s.length()-1) != '.' && s.charAt(s.length()-1) != '?' && s.charAt(s.length()-1) != '!')){
			result += s.substring(s.length()-1) + '.';
		}

		else result += s.substring(s.length()-1);
		result = s;

		return result;
	}
}