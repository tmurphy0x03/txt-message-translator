package org.translator;

/**
 * @author Anthony Murphy & Daniel Keenan
 */


import org.translator.dao.DatabaseAdapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;



public class AddWord extends Activity {

	private EditText et_txtText;
	private EditText et_engText;
	private String m_text;
	private String m_english;

	/* Called on Creation */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		/* Set the UI to add_dictionary.xml */
		setContentView(R.layout.add_dictionary);

		/* Initialise text boxes and the button */
		et_txtText = (EditText) findViewById(R.id.txt);
		et_engText = (EditText) findViewById(R.id.eng);        
		Button addDictButton = (Button) findViewById(R.id.add_translation);

		/* Read values in from Text Boxes and put into strings */
		Bundle extras = getIntent().getExtras();
		if(extras != null){   
			m_text = extras.getString(DatabaseAdapter.KEY_TXT);
			m_english = extras.getString(DatabaseAdapter.KEY_ENG);        	
			if(m_text != null){
				et_txtText.setText(m_text);
			}
			if(m_english != null){
				et_engText.setText(m_english);
			}
		}

		/* Add To Dictionary Button listener is clicked */
		addDictButton.setOnClickListener(new View.OnClickListener() {			
			public void onClick(View v) {
				/* Get entries and put into a Bundle */
				Bundle bundle = new Bundle();				
				bundle.putString(DatabaseAdapter.KEY_TXT, et_txtText.getText().toString());
				bundle.putString(DatabaseAdapter.KEY_ENG, et_engText.getText().toString());
				
				/* Add the Bundle to an Intent */
				Intent i = new Intent();
				i.putExtras(bundle);
				
				/* Return success */
				setResult(RESULT_OK, i);
				
				/* Close activity */
				finish();			
			}			
		});
	}
}