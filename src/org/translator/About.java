package org.translator;






import android.app.Activity;
import android.os.Bundle;

public class About extends Activity {
	
	/* Called on creation */
	public void onCreate(Bundle SavedInstanceState){
		super.onCreate(SavedInstanceState);
		/* Set the UI to about.xml */
		setContentView(R.layout.about);
	}
}
