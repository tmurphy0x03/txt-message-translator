package org.translator;







import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class ImportSMS extends ListActivity{
	
	private static final String KEY_INBOX = "body";
	private Cursor inboxCursor;
	
	/* Called on creation */
	public void onCreate(Bundle SavedInstanceState){
		super.onCreate(SavedInstanceState);
		
		/* Set the UI to import_sms.xml */
		setContentView(R.layout.import_sms);
		
		/* Register a context menu to be shown for a list view */ 
		registerForContextMenu(getListView());
		
		/* Query the inbox */
		Uri inbox = Uri.parse("content://sms/inbox");
		inboxCursor = getContentResolver().query(inbox, null, null ,null,null);
		
		/* Manage inboxCursor lifecycle */
		startManagingCursor(inboxCursor);

		/* Columns to use */
		String[] from = new String[]{KEY_INBOX};		

		/* Where to display the column */
		int[] to = new int[]{R.id.body};
		
		/* Map the inboxCursor to a list view */
		SimpleCursorAdapter messages = new SimpleCursorAdapter(this, R.layout.import_row, inboxCursor, from, to);
		setListAdapter(messages);
	}
	
	/* An item in the list is selected */
	protected void onListItemClick(ListView l, View v, int position, long id) {
    	super.onListItemClick(l, v, position, id);
    	
    	/* Create a cursor and move it to the selected message */
    	Cursor c = inboxCursor;
        c.moveToPosition(position);
        
        /* Add the message selected to a Bundle */
        Bundle bundle = new Bundle();
		bundle.putString("body", c.getString(c.getColumnIndexOrThrow("body")));
		
		/* Put the Bundle into an Intent */
		Intent i = new Intent();
		i.putExtras(bundle);
		
		/* Return success */
		setResult(RESULT_OK, i);
		
		/* Close the activity */
		finish();			
    }
}
