package org.translator.dao;

/**
 * @author Anthony Murphy & Daniel Keenan
 */

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseAdapter extends SQLiteOpenHelper {

	private static final String DATABASE_DIRECTORY = "/data/data/org.translator/databases/";
	private static final String DATABASE_NAME = "dictionary.sqlite";
	private static final String DATABASE_PATH = DATABASE_DIRECTORY + DATABASE_NAME;
	private static final String DATABASE_TABLE = "txteng";
	public static final String KEY_TXT = "txt";
	public static final String KEY_TXT2 = "txt2";
	public static final String KEY_TXT3 = "txt3";
	public static final String KEY_TXT4 = "txt4";
	public static final String KEY_ENG = "eng";
	public static final String KEY_ENG2 = "eng2";
	public static final String KEY_ENG3 = "eng3";
	public static final String KEY_ENG4 = "eng4";
	public static final String KEY_ROWID = "_id";
	private SQLiteDatabase db_dictionary;
	private Context m_context;
	private Cursor m_cursor;

	/* Constructor */
	public DatabaseAdapter(Context ctx) {
		super(ctx, DATABASE_NAME, null, 1);
		this.m_context = ctx;
	}

	public void onCreate(SQLiteDatabase db) {}

	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}

	/**
	 * Create a Writable Database
	 * @throws IOException
	 */
	public void createDatabase() throws IOException{
		boolean exists = checkDatabase();    	
		if(!exists){
			this.getWritableDatabase();    		
			try{
				copyDatabase();
			} catch(IOException e){
				throw new Error("Error copying database");
			}
		}
	}

	/**
	 * Check if the Database already exists
	 * @return
	 */
	private boolean checkDatabase(){
		SQLiteDatabase check = null;    	
		try{
			check = SQLiteDatabase.openDatabase(DATABASE_PATH, null, SQLiteDatabase.OPEN_READWRITE);
		} catch (SQLiteException e){}

		if(check != null){
			check.close();
			return true;
		}
		else return false;
	}

	/**
	 * Copy the data from the SQLite file to the Database
	 * @throws IOException
	 */
	private void copyDatabase() throws IOException{
		/* Stream to read from SQLite File */
		InputStream in = m_context.getAssets().open(DATABASE_NAME);
		
		/* Stream to write to the database */
		OutputStream out = new FileOutputStream(DATABASE_PATH);

		byte[] buffer = new byte[1024];
		int length;
		/* Read bytes from SQLite File and put into byte array */
		while((length = in.read(buffer)) > 0){
			/* Write byte array to the database */
			out.write(buffer, 0, length);
		}    	
		out.flush();
		out.close();
		in.close();
	}

	/**
	 * Open the Database
	 * @throws SQLException
	 */
	public void openDatabase() throws SQLException{
		db_dictionary = SQLiteDatabase.openDatabase(DATABASE_PATH, null, SQLiteDatabase.OPEN_READWRITE);
	}

	/**
	 * Close the Database
	 */
	public synchronized void close(){
		if(db_dictionary != null){
			db_dictionary.close();
		}
		super.close();
	}

	/**
	 * Add a translation to the database
	 * @param text
	 * @param text2
	 * @param text3
	 * @param text4
	 * @param english
	 * @param english2
	 * @param english3
	 * @param english4
	 * @return
	 */
	public int addTranslation(String text, String text2, String text3, String text4, String english, String english2, String english3, String english4) {
		ContentValues content = new ContentValues();
		
		/* Add the Txt Language words to content */
		content.put(KEY_TXT, text);
		if(text2 != "" && text2 != null){
			content.put(KEY_TXT2, text2);
		}
		if(text3 != "" && text3 != null){
			content.put(KEY_TXT3, text3);
		}
		if(text4 != "" && text4 != null){
			content.put(KEY_TXT4, text4);
		}
		
		/* Add the Standard English words to content */
		content.put(KEY_ENG, english);
		if(english2 != "" && english2 != null){
			content.put(KEY_ENG2, english2);
		}
		if(english3 != "" && english3 != null){
			content.put(KEY_ENG3, english3);
		}
		if(english4 != "" && english4 != null){
			content.put(KEY_ENG4, english4);
		}

		/* Insert content to the database */
		db_dictionary.insert(DATABASE_TABLE, null, content);
		return 0;
	}

	/**
	 * Query the database for a translation
	 * @param word
	 * @param word2
	 * @param word3
	 * @param word4
	 * @param direction
	 * @param initialTest
	 * @return
	 * @throws SQLException
	 */
	public Cursor getTranslation(String word, String word2, String word3, String word4, String direction, boolean initialTest) throws SQLException {
		/* Replace all ' characters with '' */
		if(word.contains("'")){
			word = word.replace("'" , "''");
		}
		if(word2 != null && word2.contains("'")){
			word2 = word2.replace("'" , "''");
		}
		if(word3 != null && word3.contains("'")){
			word3 = word3.replace("'" , "''");
		}
		if(word4 != null && word4.contains("'")){
			word4 = word4.replace("'" , "''");
		}
		
		/* Translate from Txt Message Language to Standard English */
		if(direction.equals("txteng")){
			/* Initial query */
			if(initialTest){
				m_cursor = db_dictionary.query(DATABASE_TABLE, new String[] {KEY_TXT, KEY_TXT2, KEY_TXT3, KEY_TXT4, KEY_ENG, KEY_ENG2, KEY_ENG3, KEY_ENG4}, "LOWER(" +KEY_TXT + ") = '"+word+"'", null, null, null, null, null);
			}
			/* 1 word query */
			else if(word2 == null || word2 == ""){
				m_cursor = db_dictionary.query(DATABASE_TABLE, new String[] {KEY_TXT, KEY_TXT2, KEY_TXT3, KEY_TXT4, KEY_ENG, KEY_ENG2, KEY_ENG3, KEY_ENG4}, "LOWER(" +KEY_TXT+ ") = '"+word+"'" + " AND " + KEY_TXT2 + " IS NULL", null, null, null, null, null);
			}
			/* 2 word query */
			else if(word3 == null || word3 == ""){
				m_cursor = db_dictionary.query(DATABASE_TABLE, new String[] {KEY_TXT, KEY_TXT2, KEY_TXT3, KEY_TXT4, KEY_ENG, KEY_ENG2, KEY_ENG3, KEY_ENG4}, "LOWER(" +KEY_TXT+ ") = '"+word+"'"  + " AND " + "LOWER(" + KEY_TXT2 + ") = '"+word2+"'"  + " AND " + KEY_TXT3 + " IS NULL", null, null, null, null, null);
			}
			/* 3 word query */
			else if(word4 == null || word4 == ""){
				m_cursor = db_dictionary.query(DATABASE_TABLE, new String[] {KEY_TXT, KEY_TXT2, KEY_TXT3, KEY_TXT4, KEY_ENG, KEY_ENG2, KEY_ENG3, KEY_ENG4}, "LOWER(" +KEY_TXT+ ") = '"+word+"'" + " AND " + "LOWER(" + KEY_TXT2 + ") = '"+word2+"'"  + " AND " + "LOWER(" + KEY_TXT3 + ") = '"+word3+"'"  + " AND " + KEY_TXT4 + " IS NULL", null, null, null, null, null);
			}
			/* 4 word query */
			else{
				m_cursor = db_dictionary.query(DATABASE_TABLE, new String[] {KEY_TXT, KEY_TXT2, KEY_TXT3, KEY_TXT4, KEY_ENG, KEY_ENG2, KEY_ENG3, KEY_ENG4}, "LOWER(" +KEY_TXT+ ") = '"+word+"'" + " AND " + "LOWER(" + KEY_TXT2 + ") = '"+word2+"'"  + " AND " + "LOWER(" + KEY_TXT3 + ") = '"+word3+"'"   + " AND " + "LOWER(" + KEY_TXT4 + ") = '"+word4+"'", null, null, null, null, null);
			}		
		}

		/* Translate from Standard English to Txt Message Language */
		else if(direction.equals("engtxt")){
			/* Initial query */
			if(initialTest){
				m_cursor = db_dictionary.query(DATABASE_TABLE, new String[] {KEY_ENG, KEY_ENG2, KEY_ENG3, KEY_ENG4, KEY_TXT, KEY_TXT2, KEY_TXT3, KEY_TXT4}, "LOWER(" + KEY_ENG + ") = '"+word+"'", null, null, null, null, null);
			}
			/* 1 word query */
			else if(word2 == null || word2 == ""){
				m_cursor = db_dictionary.query(DATABASE_TABLE, new String[] {KEY_ENG, KEY_ENG2, KEY_ENG3, KEY_ENG4, KEY_TXT, KEY_TXT2, KEY_TXT3, KEY_TXT4}, "LOWER(" + KEY_ENG + ") = '"+word+"'" + " AND " + KEY_ENG2 + " IS NULL", null, null, null, null, null);
			}
			/* 2 word query */
			else if(word3 == null || word3 == ""){
				m_cursor = db_dictionary.query(DATABASE_TABLE, new String[] {KEY_ENG, KEY_ENG2, KEY_ENG3, KEY_ENG4, KEY_TXT, KEY_TXT2, KEY_TXT3, KEY_TXT4}, "LOWER(" + KEY_ENG + ") = '"+word+"'"  + " AND " + "LOWER(" + KEY_ENG2 + ") = '"+word2+"'" + " AND " + KEY_ENG3 + " IS NULL", null, null, null, null, null);
			}
			/* 3 word query */
			else if(word4 == null || word4 == ""){
				m_cursor = db_dictionary.query(DATABASE_TABLE, new String[] {KEY_ENG, KEY_ENG2, KEY_ENG3, KEY_ENG4, KEY_TXT, KEY_TXT2, KEY_TXT3, KEY_TXT4}, "LOWER(" + KEY_ENG + ") = '"+word+"'" + " AND " + "LOWER(" + KEY_ENG2 + ") = '"+word2+"'"  + " AND " + "LOWER(" + KEY_ENG3 + ") = '"+word3+"'" + " AND " + KEY_ENG4 + " IS NULL", null, null, null, null, null);
			}
			/* 4 word query */
			else{
				m_cursor = db_dictionary.query(DATABASE_TABLE, new String[] {KEY_ENG, KEY_ENG2, KEY_ENG3, KEY_ENG4, KEY_TXT, KEY_TXT2, KEY_TXT3, KEY_TXT4}, "LOWER(" + KEY_ENG + ") = '"+word+"'" + " AND " + "LOWER(" + KEY_ENG2 + ") = '"+word2+"'"  + " AND " + "LOWER(" + KEY_ENG3 + ") = '"+word3+"'"   + " AND " + "LOWER(" + KEY_ENG4 + ") = '"+word4+"'", null, null, null, null, null);
			}		
		}	
		/* Move it to a random position */
		int random = (int) (Math.random() * m_cursor.getCount()) + 1;		
		if (m_cursor != null) {
			if(m_cursor.moveToPosition(random) == false){
				m_cursor.moveToFirst();
			}			
		}
		return m_cursor;
	}	
}
